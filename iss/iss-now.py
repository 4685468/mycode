#!/usr/bin/python3
import requests
import datetime
import reverse_geocoder

issNow = 'http://api.open-notify.org/iss-now.json'

def main():

    preJSONfile = requests.get(issNow)
    JSONfile = preJSONfile.json()
    currTime = datetime.datetime.fromtimestamp(JSONfile['timestamp'])
    coordsTuple = (JSONfile['iss_position']['latitude'], JSONfile['iss_position']['longitude'])
    result = reverse_geocoder.search(coordsTuple)
   
    print("CURRENT LOCATION OF THE ISS:")
    print(f"Timestamp: {currTime}")
    print(f"Lon: {JSONfile['iss_position']['longitude']}")
    print(f"Lat: {JSONfile['iss_position']['latitude']}")
    print(f"City/Country: {result[0]['name']}, {result[0]['cc']}")

if __name__ == "__main__":
    main()
